<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
class ApiController extends Controller
{
    public function mobileLogin (Request $request){
    	$userInfo = User::where('email', $request->email)->first();
    	if(Hash::check($request->password, $userInfo->password)){
    		return response()->json(['data' => $userInfo, 'message' => 'Login Successful']);
    	}
    	return response()->json(['data' => '', 'message' => 'Login Failed']);
    }
}
