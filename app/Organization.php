<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
    	'organization_name','organization_email','organization_logo','organization_phone','organization_address','role_id','password'
    	];
}
