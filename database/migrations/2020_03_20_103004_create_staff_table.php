<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->id();
            $table->string('staff_name');
            $table->string('staff_email')->unique();
            $table->text('staff_image')->nullable();
            $table->text('staff_liscence_image');
            $table->integer('staff_phone_number');
            $table->bigInteger('organization_id')->unsigned();
            $table->string('staff_bike_number');
            $table->text('staff_mobile_id');
            $table->text('staff_address')->nullable();
            $table->integer('role_id');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
