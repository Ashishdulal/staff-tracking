<?php

use Illuminate\Database\Seeder;
use App\Organization;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Organization::create([
        	'organization_name'=>'abc company',
        	'organization_email'=>'organization@organization.com',
        	'organization_logo'=> 'company_logo.png',
        	'organization_phone'=>'0987654321',
        	'organization_address'=>'asdfgh',
        	'role_id'=>'2',
        	'password'=>bcrypt('organization'),
        ]);
    }
}
