<?php

use Illuminate\Database\Seeder;
use App\Staff;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Staff::create([
        	'staff_name'=>'Staff1',
			'staff_image'=>'staff-image.jpg',
			'staff_email'=>'staff@staff.com',
			'staff_liscence_image'=>'staff-liscence.jpg',
			'staff_phone_number'=>'0987654321',
			'organization_id'=>'1',
			'staff_bike_number'=>'BA 12 pa 1234',
			'staff_mobile_id'=>'123456789987654321',
			'staff_address'=>'asdfgh',
			'role_id'=>'3',
			'password'=> bcrypt('staff')
        ]);
        Staff::create([
        	'staff_name'=>'Staff2',
			'staff_image'=>'staff-image1.jpg',
			'staff_email'=>'staff2@staff.com',
			'staff_liscence_image'=>'staff-liscence1.jpg',
			'staff_phone_number'=>'0987654321',
			'organization_id'=>'1',
			'staff_bike_number'=>'BA 11 pa 3456',
			'staff_mobile_id'=>'6789987654321323',
			'staff_address'=>'qwrer',
			'role_id'=>'3',
			'password'=> bcrypt('staff')
        ]);
    }
}
