<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = new User();
        $users->name = 'admin';
        $users->email = 'admin@admin.com';
        $users->admin_image = 'admin-image.png';
        $users->role_id = '1';
        $users->password = bcrypt('admin');
        $users->save();
    }
}
